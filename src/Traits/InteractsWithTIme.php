<?php

namespace Finoghentov\Cache\Traits;

use DateInterval;
use Finoghentov\Cache\Date;

trait InteractsWithTIme
{
    /**
     * @param DateInterval $date_interval
     * @return int seconds
     * @throws \Exception
     */
    function dateIntervalToSeconds(DateInterval $date_interval): int
    {
        return Date::dateIntervalToSeconds($date_interval);
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function currentTime(): int
    {
        return Date::currentTime();
    }
}
