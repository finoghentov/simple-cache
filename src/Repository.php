<?php


namespace Finoghentov\Cache;


use Closure;
use DateInterval;
use Finoghentov\Cache\Contracts\RepositoryInterface;
use Finoghentov\Cache\Contracts\StorageInterface;
use Finoghentov\Cache\Traits\InteractsWithTIme;

class Repository implements RepositoryInterface
{
    use InteractsWithTIme;

    /**
     * @var StorageInterface
     */
    protected StorageInterface $storage;

    /**
     * Repository constructor.
     *
     * @param StorageInterface $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        $value = $this->storage->get($key);

        if (is_null($value)) {
            return $default;
        }

        return $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param null|int|DateInterval $ttl
     * @return bool
     * @throws \Exception
     */
    public function set($key, $value, $ttl = null): bool
    {
        $time = $this->getSeconds($ttl);

        return $this->storage->put($key, $value, $time);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete($key): bool
    {
        return $this->storage->forget($key);
    }

    /**
     * @return bool
     */
    public function clear(): bool
    {
        return $this->storage->flush();
    }

    /**
     * @param array $keys
     * @param null $default
     * @return array
     */
    public function getMultiple($keys, $default = null): array
    {
        $results = [];

        foreach ($this->storage->many($keys) as $value) {
            $results[] = $value ?? $default;
        }

        return $results;
    }

    /**
     * @param array $values
     * @param null $ttl
     * @return bool
     */
    public function setMultiple($values, $ttl = null): bool
    {
       return $this->storage->putMany($values, $ttl);
    }

    /**
     * @param iterable $keys
     * @return bool
     */
    public function deleteMultiple($keys): bool
    {
        $results = [];

        foreach ($keys as $key) {
            $results[] = $this->delete($key);
        }

        if (in_array(false, $results)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key): bool
    {
        return (bool) $this->get($key);
    }

    /**
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    /**
     * @param null|int|DateInterval $time
     *
     * @return int
     * @throws \Exception
     */
    protected function getSeconds($time): int
    {
        if ($time instanceof DateInterval) {
            $time = $this->dateIntervalToSeconds($time);
        }

        return $time > 0 ? time() + $time : 0;
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function increment(string $key, int $value = 1): bool
    {
        return $this->storage->increment($key, $value);
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function decrement(string $key, int $value = 1): bool
    {
        return $this->storage->decrement($key, $value);
    }

    /**
     * @param string $key
     * @param int|null|\DateInterval $ttl
     * @param Closure $callable
     * @return mixed
     * @throws \Exception
     */
    public function remember(string $key, $ttl, Closure $callable)
    {
        $value = $this->get($key);

        if (is_null($value)) {
            $this->set($key, $result = $callable(), $ttl);

            return $result;
        }

        return $value;
    }

    /**
     * @param string $key
     * @param Closure $callable
     * @return mixed
     * @throws \Exception
     */
    public function rememberForever(string $key, Closure $callable)
    {
        return $this->remember($key, 0, $callable);
    }
}
