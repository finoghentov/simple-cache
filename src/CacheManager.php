<?php

namespace Finoghentov\Cache;

use Finoghentov\Cache\Contracts\RepositoryInterface;
use Finoghentov\Cache\Contracts\StorageInterface;
use Finoghentov\Cache\Exceptions\CacheManagerException;
use Finoghentov\Cache\Storages\FileStorage;
use Finoghentov\Cache\Storages\MemcacheStorage;
use Finoghentov\Cache\Storages\RedisStorage;
use Memcache;
use Redis;

class CacheManager
{
    const FILE_DRIVER = 'file';
    const MEMCACHE_DRIVER = 'memcache';
    const REDIS_DRIVER = 'redis';

    /**
     * @var array<RepositoryInterface>
     */
    private array $repositories = [];

    /**
     * @param string $driver
     * @param array $config
     * @return RepositoryInterface
     * @throws CacheManagerException
     */
    public function resolve(string $driver, array $config = []): RepositoryInterface
    {
        if ($this->repositoryExists($driver)) {
            return $this->getRepository($driver);
        }

        $driverCallMethod = "create" . ucfirst($driver) . "Driver";

        if (method_exists($this, $driverCallMethod)) {
            return $this->repositories[$driver] = $this->{$driverCallMethod}($config);
        } else {
            throw new CacheManagerException(
                "Driver [{$driver}] is not supportable by " . self::class . '.' . PHP_EOL .
                 "You can implement your driver using [customDriver] method."
            );
        }
    }

    /**
     * Creating of custom cache driver
     *
     * @param string $driver
     * @param StorageInterface $storage
     * @return RepositoryInterface
     */
    public function createCustomDriver(string $driver, StorageInterface $storage): RepositoryInterface
    {
        if ($this->repositoryExists($driver)) {
            return $this->getRepository($driver);
        }

        return $this->repositories[$driver] = $this->makeRepository($storage);
    }

    /**
     * @param array $config
     * @return RepositoryInterface
     */
    protected function createFileDriver(array $config): RepositoryInterface
    {
        $application_root = $_SERVER['DOCUMENT_ROOT'];

        if ($this->runningUsingCli()) {
            $application_root = $_SERVER['PWD'];
        }

        $base_path = $config['base_path']
            ?? $application_root . DIRECTORY_SEPARATOR . 'cache/';

        $permissions = $config['permissions'] ?? '0777';

        return $this->makeRepository(new FileStorage($base_path, $permissions));
    }

    /**
     * @param array $config
     * @return RepositoryInterface
     */
    protected function createMemcacheDriver(array $config): RepositoryInterface
    {
        $host = $config['host'] ?? '127.0.0.1';
        $port = $config['port'] ?? '11211';
        $memcache = new Memcache();
        $memcache->connect($host, $port);

        return $this->makeRepository(new MemcacheStorage($memcache));
    }

    /**
     * @param array $config
     * @return RepositoryInterface
     */
    protected function createRedisDriver(array $config): RepositoryInterface
    {
        $host = $config['host'] ?? '127.0.0.1';
        $port = $config['port'] ?? '11211';
        $redis = new Redis();

        $redis->connect($host, $port);

        if (isset($config['password'])) {
            $redis->auth($config['password']);
        }

        return $this->makeRepository(new RedisStorage($redis));
    }

    /**
     * @param StorageInterface $storage
     * @return RepositoryInterface
     */
    protected function makeRepository(StorageInterface $storage): RepositoryInterface
    {
        return new Repository($storage);
    }

    /**
     * @param string $driver
     * @return bool
     */
    protected function repositoryExists(string $driver): bool
    {
        return isset($this->repositories[$driver]);
    }

    /**
     * @param string $driver
     * @return ?RepositoryInterface
     */
    public function getRepository(string $driver): ?RepositoryInterface
    {
        return $this->repositories[$driver] ?? null;
    }

    /**
     * @return bool
     */
    private function runningUsingCli(): bool
    {
        if (
            empty($_SERVER['REMOTE_ADDR'])
            && !isset($_SERVER['HTTP_USER_AGENT'])
            && count($_SERVER['argv']) > 0
            || defined('STDIN')
        ) {
            return true;
        }

        return false;
    }
}
