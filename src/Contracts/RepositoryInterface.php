<?php

namespace Finoghentov\Cache\Contracts;

use Closure;
use Psr\SimpleCache\CacheInterface;

interface RepositoryInterface extends CacheInterface
{
    /**
     * @param string $key
     * @param int $value
     * @return bool
     */
    public function increment(string $key, int $value = 1): bool;

    /**
     * @param string $key
     * @param int $value
     * @return bool
     */
    public function decrement(string $key, int $value = 1): bool;

    /**
     * @param string $key
     * @param int|null|\DateInterval $ttl
     * @param Closure $callable
     * @return mixed
     */
    public function remember(string $key, $ttl, Closure $callable);

    /**
     * @param string $key
     * @param Closure $callable
     * @return mixed
     */
    public function rememberForever(string $key, Closure $callable);

    /**
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface;
}
