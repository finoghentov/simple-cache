<?php

namespace Finoghentov\Cache;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;

class Date
{
    /**
     * @var string|DateTimeInterface|DateInterval
     */
    protected static $now = "now";

    /**
     * @var bool
     */
    protected static bool $testing = false;

    /**
     * @param DateInterval $date_interval
     * @return int
     * @throws \Exception
     */
    public static function dateIntervalToSeconds(DateInterval $date_interval): int
    {
        $reference = new DateTimeImmutable(self::$now);
        $end_time = $reference->add($date_interval);

        return $end_time->getTimestamp() - $reference->getTimestamp();
    }

    /**
     * @return int
     * @throws Exception
     */
    public static function currentTime(): int
    {
        if (self::$now instanceof DateTime) {
            $date_time = DateTimeImmutable::createFromMutable(self::$now);
        } else {
            $date_time = new \DateTimeImmutable(self::$now);
        }

        return $date_time->getTimestamp();
    }

    /**
     * Only for testing environment
     */
    public static function testing(): void
    {
        self::$testing = true;
    }

    /**
     * Only for testing environment
     *
     * @param string|DateTimeInterface|DateInterval $date
     */
    public static function setNow($date): void
    {
        if (!self::$testing) {
            return;
        }

        if ($date instanceof DateInterval) {
            $current_date = new DateTimeImmutable();
            $date = $current_date->add($date)->format('Y-m-d h:i:s');
        }

        self::$now = $date;
    }
}
