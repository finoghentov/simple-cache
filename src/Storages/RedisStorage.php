<?php

namespace Finoghentov\Cache\Storages;

use Finoghentov\Cache\Contracts\StorageInterface;
use Redis;

class RedisStorage implements StorageInterface
{
    /**
     * @var Redis with connection
     */
    protected Redis $redis;

    /**
     * MemcacheStorage constructor.
     *
     * @param Redis $redis
     */
    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Retrieve an item from the cache by key.
     *
     * @param string|array $key
     * @return mixed
     */
    public function get($key)
    {
        $value = $this->redis->get($key);

        if ($value === false) {
            return null;
        }
        try {
            return unserialize($value);
        } catch (\Exception $exception) {
            if (is_numeric($value) && $value[0] != 0) {
                $value = (int) $value;
            }

            return $value;
        }
    }

    /**
     * Retrieve multiple items from the cache by key.
     *
     * Items not found in the cache will have a null value.
     *
     * @param array $keys
     * @return array
     */
    public function many(array $keys): array
    {
        $results = [];

        foreach ($keys as $key) {
            $results[] = $this->get($key);
        }

        return $results;
    }

    /**
     * Store an item in the cache for a given number of seconds.
     *
     * @param string $key
     * @param mixed $value
     * @param int $seconds
     * @return bool
     */
    public function put($key, $value, $seconds): bool
    {
        if ($seconds === 0) {
            $seconds = null;
        } else {
            $seconds = $seconds - time();
        }

        if (is_array($value) || is_object($value)) {
            $value = serialize($value);
        }

        return $this->redis->set($key, $value, $seconds);
    }

    /**
     * Store multiple items in the cache for a given number of seconds.
     *
     * @param array $values
     * @param int $seconds
     * @return bool
     */
    public function putMany(array $values, $seconds): bool
    {
        $results = [];

        foreach ($values as $key => $value) {
            $results[] = $this->put($key, $value, $seconds);
        }

        if (in_array(false, $results)) {
            foreach ($values as $key => $value) {
                $this->forget($key);
            }

            return false;
        }

       return true;
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function increment(string $key, int $value = 1): bool
    {
        return $this->redis->incrBy($key, $value);
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function decrement(string $key, int $value = 1): bool
    {
        return $this->redis->decrBy($key, $value);
    }

    /**
     * Remove an item from the cache.
     *
     * @param string $key
     * @return bool
     */
    public function forget($key): bool
    {
        return $this->redis->del($key);
    }

    /**
     * Remove all items from the cache.
     *
     * @return bool
     */
    public function flush(): bool
    {
        return $this->redis->flushAll();
    }

    /**
     * @param string $key
     * @return array
     */
    public function getPayload(string $key): array
    {
        return [
            'data' => $this->get($key)
        ];
    }
}
