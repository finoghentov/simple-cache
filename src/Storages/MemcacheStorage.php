<?php

namespace Finoghentov\Cache\Storages;

use Finoghentov\Cache\Contracts\StorageInterface;
use Memcache;

class MemcacheStorage implements StorageInterface
{
    /**
     * @var Memcache with connection
     */
    protected Memcache $memcache;

    /**
     * MemcacheStorage constructor.
     *
     * @param Memcache $memcache
     */
    public function __construct(Memcache $memcache)
    {
        $this->memcache = $memcache;
    }

    /**
     * Retrieve an item from the cache by key.
     *
     * @param string|array $key
     * @return mixed
     */
    public function get($key)
    {
        $value = $this->memcache->get($key);

        if ($value === false) {
            return null;
        }

        return $value;
    }

    /**
     * Retrieve multiple items from the cache by key.
     *
     * Items not found in the cache will have a null value.
     *
     * @param array $keys
     * @return array
     */
    public function many(array $keys): array
    {
        $results = [];

        foreach ($keys as $key) {
            $results[] = $this->get($key);
        }

        return $results;
    }

    /**
     * Store an item in the cache for a given number of seconds.
     *
     * @param string $key
     * @param mixed $value
     * @param int $seconds
     * @return bool
     */
    public function put($key, $value, $seconds): bool
    {
        if ($seconds > 0) {
            $seconds = $seconds - time();
        }

        if ($this->memcache->get($key)) {
            return $this->memcache->replace($key, $value, null, $seconds);
        }

        return $this->memcache->set($key, $value, null, $seconds);
    }

    /**
     * Store multiple items in the cache for a given number of seconds.
     *
     * @param array $values
     * @param int $seconds
     * @return bool
     */
    public function putMany(array $values, $seconds): bool
    {
       return $this->memcache->setMulti($values, $seconds);
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function increment(string $key, int $value = 1): bool
    {
        return $this->memcache->increment($key, $value);
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function decrement(string $key, int $value = 1): bool
    {
        return $this->memcache->decrement($key, $value);
    }

    /**
     * Remove an item from the cache.
     *
     * @param string $key
     * @return bool
     */
    public function forget($key): bool
    {
        return $this->memcache->delete($key);
    }

    /**
     * Remove all items from the cache.
     *
     * @return bool
     */
    public function flush(): bool
    {
        return $this->memcache->flush();
    }

    /**
     * @param string $key
     * @return array
     */
    public function getPayload(string $key): array
    {
        return [
            'data' => $this->get($key)
        ];
    }
}
