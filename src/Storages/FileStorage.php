<?php

namespace Finoghentov\Cache\Storages;

use Finoghentov\Cache\Contracts\StorageInterface;
use Finoghentov\Cache\Traits\InteractsWithTIme;

class FileStorage implements StorageInterface
{
    use InteractsWithTIme;

    /**
     * @var string
     */
    protected string $base_path;

    /**
     * @var string
     */
    protected string $permission;

    /**
     * FileStorage constructor.
     *
     * @param string $base_path
     * @param string $permission in octal representation
     */
    public function __construct(string $base_path, string $permission)
    {
        $this->base_path = $base_path;
        $this->permission = $permission;
    }

    /**
     * Retrieve an item from the cache by key
     *
     * @param $key
     * @return mixed
     * @throws \Exception
     */
    public function get($key)
    {
        $path = $this->pathTo($key);

        try {
            $expiration_time = substr(
                $content = file_get_contents($path), 0, 10
            );
        } catch (\Exception $e) {
            $this->forget($key);

            return null;
        }

        if ($this->currentTime() >= $expiration_time) {
            $this->forget($key);

            return null;
        }

        try {
            return unserialize(
                substr($content, 10)
            );
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param string $key
     * @return array
     */
    public function getPayload(string $key): array
    {
        $path = $this->pathTo($key);

        try {
            $expiration_time = substr(
                $content = file_get_contents($path), 0, 10
            );

            $content = unserialize(
                substr($content, 10)
            );

            return [
                'data' => $content,
                'expired_at' => $expiration_time
            ];
        } catch (\Exception $exception) {
            return [
                'data' => null,
                'expired_at' => null
            ];
        }
    }

    /**
     * Retrieve multiple items from the cache by key.
     *
     * Items not found in the cache will have a null value.
     *
     * @param array $keys
     * @return array
     * @throws \Exception
     */
    public function many(array $keys): array
    {
        $results = [];

        foreach ($keys as $key) {
            $results[] = $this->get($key);
        }

        return $results;
    }

    /**
     * Store an item in the cache for a given number of seconds.
     *
     * @param string $key
     * @param mixed $value
     * @param int $seconds
     * @return bool
     */
    public function put($key, $value, $seconds): bool
    {
        $path = $this->pathTo($key);

        $result = file_put_contents(
            $path, $this->expiration($seconds) . serialize($value)
        );

        if ($result !== false && $result > 0) {
            chmod($path, octdec($this->permission));
        }

        return $result;
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function increment(string $key, int $value = 1): bool
    {
        $payload = $this->getPayload($key);

        if (is_null($payload['data'])) {
            return $this->put($key, $value, $this->expiration());
        }

        return $this->put($key, $payload['data'] + $value, $payload['expired_at']);
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function decrement(string $key, int $value = 1): bool
    {
        $payload = $this->getPayload($key);

        if (is_null($payload['data'])) {
            return $this->put($key, $value, $this->expiration());
        }

        return $this->put($key, $payload['data'] - $value, $payload['expired_at']);
    }

    /**
     * Store multiple items in the cache for a given number of seconds.
     *
     * @param array $values
     * @param int $seconds
     * @return bool
     */
    public function putMany(array $values, $seconds): bool
    {
        $result = [];

        foreach ($values as $key => $value) {
            $result[] = $this->put($key, $value, $seconds);
        }

        if (in_array(false, $result)) {
            foreach ($values as $key => $value) {
                $this->forget($key);
            }

            return false;
        }

        return true;
    }

    /**
     * Remove an item from the cache.
     *
     * @param string $key
     * @return bool
     */
    public function forget($key): bool
    {
        $path = $this->pathTo($key);

        try {
            if (is_file($path)) {
                unlink($path);
            }

            $segments = explode('/', $path);

            array_pop($segments);

            $this->clearCacheDirectories(implode('/', $segments));

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Remove all items from the cache.
     *
     * @return bool
     */
    public function flush(): bool
    {
        if (!$this->cacheDirectoryExistsWithValidPermissions()) {
            return true;
        }

        try {
            $this->clearCache();

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }


    /**
     * @param string $key
     * @return string
     */
    protected function pathTo(string $key): string
    {
        if (!$this->cacheDirectoryExistsWithValidPermissions()) {
            $this->makeCacheDirectory();
        }

        $segments = array_slice(str_split($hash = sha1($key), 4), 0, 2);

        $full_path = $this->base_path . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $segments);

        if (!is_dir($full_path)) {
            mkdir($full_path, octdec($this->permission), true);
        }

        return str_replace('//', '/', $full_path . DIRECTORY_SEPARATOR . $hash);
    }

    /**
     * @return bool
     */
    protected function cacheDirectoryExistsWithValidPermissions(): bool
    {
        return is_dir($this->base_path) && substr(sprintf('%o', fileperms($this->base_path)), -4);
    }

    /**
     * @return bool
     */
    protected function makeCacheDirectory(): bool
    {
        return mkdir($this->base_path, octdec($this->permission));
    }

    /**
     * @param int $seconds
     * @return int
     */
    protected function expiration(int $seconds = 0): int
    {
        return $seconds === 0 ? 9999999999 : $seconds;
    }

    protected function clearCache(): void
    {
        $this->recursive_deleting($this->base_path);
    }

    /**
     * @param string $path
     */
    protected function clearCacheDirectories(string $path): void
    {
        if (is_dir($path) && count(scandir($path)) === 2) {
            rmdir($path);

            $segments = explode('/', $path);

            array_pop($segments);

            $this->clearCacheDirectories(implode('/', $segments));
        }
    }

    /**
     * @param string $target
     * @param bool $delete_root
     */
    protected function recursive_deleting(string $target, bool $delete_root = true): void
    {
        if (is_dir($target)) {
            $objects = glob($target . '*', GLOB_MARK);

            foreach ($objects as $object) {
                $this->recursive_deleting($object);
            }

            if ($delete_root) {
                rmdir($target);
            }
        } elseif (is_file($target)) {
            unlink($target);
        }
    }
}
