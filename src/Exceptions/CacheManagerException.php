<?php


namespace Finoghentov\Cache\Exceptions;

use Psr\SimpleCache\CacheException;

class CacheManagerException extends \Exception implements CacheException
{

}
