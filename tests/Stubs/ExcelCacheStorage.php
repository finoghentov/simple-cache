<?php


namespace Finoghentov\Cache\Tests\Stubs;


use Finoghentov\Cache\Contracts\StorageInterface;

class ExcelCacheStorage implements StorageInterface
{
    /**
     * Retrieve an item from the cache by key
     *
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        // TODO: Implement get() method.
    }

    /**
     * Retrieve multiple items from the cache by key.
     *
     * Items not found in the cache will have a null value.
     *
     * @param array $keys
     * @return array
     */
    public function many(array $keys)
    {
        // TODO: Implement many() method.
    }

    /**
     * Store an item in the cache for a given number of seconds.
     *
     * @param string $key
     * @param mixed $value
     * @param int $seconds
     * @return bool
     */
    public function put($key, $value, $seconds)
    {
        // TODO: Implement put() method.
    }

    /**
     * Store multiple items in the cache for a given number of seconds.
     *
     * @param array $values
     * @param int $seconds
     * @return bool
     */
    public function putMany(array $values, $seconds)
    {
        // TODO: Implement putMany() method.
    }


    /**
     * Remove an item from the cache.
     *
     * @param string $key
     * @return bool
     */
    public function forget($key)
    {
        // TODO: Implement forget() method.
    }

    /**
     * Remove all items from the cache.
     *
     * @return bool
     */
    public function flush()
    {
        // TODO: Implement flush() method.
    }

    /**
     * Get the cache key prefix.
     *
     * @return string
     */
    public function getPrefix()
    {
        // TODO: Implement getPrefix() method.
    }

    /**
     * @param string $key
     * @return array
     */
    public function getPayload(string $key): array
    {
        // TODO: Implement getPayload() method.
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function increment(string $key, int $value = 1): bool
    {
        // TODO: Implement increment() method.
    }

    /**
     * @param string $key
     * @param int $value
     * @return bool
     * @throws \Exception
     */
    public function decrement(string $key, int $value = 1): bool
    {
        // TODO: Implement decrement() method.
    }
}
