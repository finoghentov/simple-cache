<?php

namespace Finoghentov\Cache\Tests\Unit;

use Finoghentov\Cache\CacheManager;
use Finoghentov\Cache\Contracts\RepositoryInterface;
use Finoghentov\Cache\Exceptions\CacheManagerException;
use Finoghentov\Cache\Storages\FileStorage;
use Finoghentov\Cache\Storages\MemcacheStorage;
use Finoghentov\Cache\Tests\Stubs\ExcelCacheStorage;
use PHPUnit\Framework\TestCase;

/**
 * @covers CacheManager
 */
class CacheManagerTest extends TestCase
{
    /**
     * @test
     * @group manager
     */
    public function can_resolve_file_cache_repository()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);
        $this->assertInstanceOf(RepositoryInterface::class, $repository);
        $this->assertInstanceOf(FileStorage::class, $repository->getStorage());
        $this->assertSame($repository, $manager->resolve(CacheManager::FILE_DRIVER));
    }

    /**
     * @test
     * @group manager
     */
    public function cannot_resolve_undefined_driver()
    {
        $manager = new CacheManager();
        $this->expectException(CacheManagerException::class);
        $manager->resolve('invalid_driver');
    }

    /**
     * @test
     * @group manager
     */
    public function can_resolve_custom_driver()
    {
        $manager = new CacheManager();
        $repository = $manager->createCustomDriver('excel', new ExcelCacheStorage());
        $this->assertInstanceOf(ExcelCacheStorage::class, $repository->getStorage());
    }

    /**
     * @test
     * @group manager
     */
    public function manager_can_have_different_repository_instances()
    {
        $manager = new CacheManager();
        $manager->createCustomDriver('excel', new ExcelCacheStorage());
        $manager->resolve(CacheManager::FILE_DRIVER);
        $manager->resolve(CacheManager::MEMCACHE_DRIVER);

        $this->assertInstanceOf(
            FileStorage::class,
            $manager->getRepository(CacheManager::FILE_DRIVER)->getStorage()
        );

        $this->assertInstanceOf(
            ExcelCacheStorage::class,
            $manager->getRepository('excel')->getStorage()
        );

        $this->assertInstanceOf(
            MemcacheStorage::class,
            $manager->getRepository(CacheManager::MEMCACHE_DRIVER)->getStorage()
        );
    }
}
