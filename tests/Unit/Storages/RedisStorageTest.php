<?php

namespace Finoghentov\Cache\Tests\Unit\Storages;

use DateTime;
use Finoghentov\Cache\CacheManager;
use Finoghentov\Cache\Date;
use Finoghentov\Cache\Repository;
use Finoghentov\Cache\Storages\RedisStorage;
use Finoghentov\Cache\Tests\Stubs\UserDTO;
use PHPUnit\Framework\TestCase;

/**
 * @covers Repository|RedisStorage
 */
class RedisStorageTest extends TestCase
{
    protected array $config;

    protected function setUp(): void
    {
        parent::setUp();

        $this->config = [
            'host' => $_ENV['REDIS_HOST'],
            'port' => $_ENV['REDIS_PORT'],
            'password' => $_ENV['REDIS_PASSWORD'],
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);
        $repository->clear();
        Date::setNow('now');
    }

    /**
     * @test
     * @group redis
     */
    public function manager_can_resolve_cache_storage()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);
        $this->assertInstanceOf(RedisStorage::class, $repository->getStorage());

        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);
        $repository->set('foo', 'bar');
        $repository->clear();
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_set_different_types_of_values_to_cache()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        /*
         * int
         */
        $result = $repository->set('integer', 1);
        $this->assertIsInt($repository->get('integer'));
        $this->assertTrue($result);

        /*
         * string
         */
        $result = $repository->set('string', 'value');
        $this->assertIsString($repository->get('string'));
        $this->assertTrue($result);

        /*
         * numeric array
         */
        $result = $repository->set('list', [1, 2, 3]);
        $this->assertTrue($result);
        $this->assertIsArray($repository->get('list'));

        /*
         * Hash Map
         */
        $result = $repository->set('hash_map', [
            'key' => 'value',
            'foo' => 'bar'
        ]);
        $this->assertTrue($result);
        $this->assertIsArray($repository->get('hash_map'));

        /*
         * Object
         */
        $object = new UserDTO('Name1', 'Email1');
        $result = $repository->set('object', $object);
        $this->assertTrue($result);
        $this->assertInstanceOf(UserDTO::class, $repository->get('object'));
        $manager->resolve(CacheManager::FILE_DRIVER);
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_get_cached_value()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        $repository->set('foo', 'bar');
        $foo = $repository->get('foo');
        $this->assertEquals('bar', $foo);

        $users = [
            new UserDTO('Name1', 'Email1'),
            new UserDTO('Name2', 'Email2'),
            new UserDTO('Name3', 'Email3'),
        ];

        $repository->set('users', $users);
        $this->assertContainsOnly(UserDTO::class, $repository->get('users'));

        $this->assertEquals('default_value', $repository->get('not_existed_key', 'default_value'));
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_get_multiple_values()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        $repository->set('foo', 'bar');
        $repository->set('key', 'value');

        $this->assertSame([
            'bar', 'value', null
        ], $repository->getMultiple(['foo', 'key', 'not_existing_key']));
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_forget_multiple_keys()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        $repository->set('foo', 'bar');
        $repository->set('key', 'value');

        $this->assertSame([
            'bar', 'value'
        ], $repository->getMultiple(['foo', 'key']));

        $repository->deleteMultiple([
            'foo', 'key'
        ]);

        $this->assertSame([
            null, null
        ], $repository->getMultiple(['foo', 'key']));
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_forget_cache()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);
        $repository->set('foo', 'bar');
        $result = $repository->delete('foo');
        $this->assertTrue($result);
        $this->assertNull($repository->get('foo'));
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_flush_all_cache()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        $repository->set('foo', 'bar');
        $repository->set('key', 'value');
        $repository->clear();
        $this->assertNull($repository->get('foo'));
        $this->assertNull($repository->get('key'));
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_set_value_to_cache_for_specific_time()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        $repository->set('foo', 'bar', 1);
        $this->assertEquals('bar', $repository->get('foo'));
        sleep(1);
        $this->assertNull($repository->get('foo'));
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_increment_and_decrement_cache_value()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        $repository->set('value', 5);

        $repository->increment('value');
        $this->assertEquals(6, $repository->get('value'));

        $repository->decrement('value');
        $this->assertEquals(5, $repository->get('value'));

        $repository->increment('value', 2);
        $this->assertEquals(7, $repository->get('value'));

        $repository->decrement('value', 2);
        $this->assertEquals(5, $repository->get('value'));
    }

    /**
     * @test
     * @group redis
     */
    public function repository_can_remember_and_retrieve_exists_and_create_non_exists_data()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::REDIS_DRIVER, $this->config);

        $result = $repository->rememberForever('foo', function () {
            return 'bar';
        });

        $this->assertEquals('bar', $result);
        $this->assertEquals('bar', $repository->get('foo'));

        $repository->set('name', 'Pavel');

        $result = $repository->rememberForever('name', function () {
           return 'Finoghentov';
        });

        $this->assertEquals('Pavel', $result);

        $repository->remember('will_expire', 1, function () {
            return 'expired';
        });

        sleep(2);
        $this->assertNull($repository->get('will_expire'));
    }
}
