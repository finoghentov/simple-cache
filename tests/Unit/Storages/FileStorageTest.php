<?php

namespace Finoghentov\Cache\Tests\Unit\Storages;

use DateTime;
use Finoghentov\Cache\CacheManager;
use Finoghentov\Cache\Date;
use Finoghentov\Cache\Repository;
use Finoghentov\Cache\Storages\FileStorage;
use Finoghentov\Cache\Tests\Stubs\UserDTO;
use PHPUnit\Framework\TestCase;

/**
 * @covers Repository|FileStorage
 */
class FileStorageTest extends TestCase
{
    protected function tearDown(): void
    {
        parent::tearDown();
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);
        $repository->clear();
        Date::setNow('now');
    }

    /**
     * @test
     * @group file
     */
    public function manager_can_resolve_cache_storage()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);
        $this->assertInstanceOf(FileStorage::class, $repository->getStorage());

        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER, [
            'base_path' => $_SERVER['PWD'] . '/custom-cache'
        ]);
        $repository->set('foo', 'bar');
        $this->assertDirectoryExists($_SERVER['PWD'] . '/custom-cache');
        $repository->clear();
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_set_different_types_of_values_to_cache()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);

        /*
         * int
         */
        $result = $repository->set('integer', 1);
        $this->assertIsInt($repository->get('integer'));
        $this->assertTrue($result);

        /*
         * string
         */
        $result = $repository->set('string', 'value');
        $this->assertIsString($repository->get('string'));
        $this->assertTrue($result);

        /*
         * numeric array
         */
        $result = $repository->set('list', [1, 2, 3]);
        $this->assertTrue($result);
        $this->assertIsArray($repository->get('list'));

        /*
         * Hash Map
         */
        $result = $repository->set('hash_map', [
            'key' => 'value',
            'foo' => 'bar'
        ]);
        $this->assertTrue($result);
        $this->assertIsArray($repository->get('hash_map'));

        /*
         * Object
         */
        $object = new UserDTO('Name1', 'Email1');
        $result = $repository->set('object', $object);
        $this->assertTrue($result);
        $this->assertInstanceOf(UserDTO::class, $repository->get('object'));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_get_cached_value()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);

        $repository->set('foo', 'bar');
        $foo = $repository->get('foo');
        $this->assertEquals('bar', $foo);

        $users = [
            new UserDTO('Name1', 'Email1'),
            new UserDTO('Name2', 'Email2'),
            new UserDTO('Name3', 'Email3'),
        ];

        $repository->set('users', $users);
        $this->assertContainsOnly(UserDTO::class, $repository->get('users'));

        $this->assertEquals('default_value', $repository->get('not_existed_key', 'default_value'));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_get_multiple_values()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);

        $repository->set('foo', 'bar');
        $repository->set('key', 'value');

        $this->assertSame([
            'bar', 'value', null
        ], $repository->getMultiple(['foo', 'key', 'not_existing_key']));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_forget_multiple_keys()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);

        $repository->set('foo', 'bar');
        $repository->set('key', 'value');

        $this->assertSame([
            'bar', 'value'
        ], $repository->getMultiple(['foo', 'key']));

        $repository->deleteMultiple([
            'foo', 'key'
        ]);

        $this->assertSame([
            null, null
        ], $repository->getMultiple(['foo', 'key']));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_forget_cache()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);
        $repository->set('foo', 'bar');
        $result = $repository->delete('foo');
        $this->assertTrue($result);
        $this->assertNull($repository->get('foo'));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_flush_all_cache()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER, [
            'base_path' => $_SERVER['PWD'] . '/custom_cache_directory'
        ]);

        $repository->set('foo', 'bar');
        $repository->set('key', 'value');
        $this->assertDirectoryExists($_SERVER['PWD'] . '/custom_cache_directory');
        $repository->clear();
        $this->assertDirectoryDoesNotExist($_SERVER['PWD'] . '/custom_cache_directory');
        $this->assertNull($repository->get('foo'));
        $this->assertNull($repository->get('key'));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_set_value_to_cache_for_specific_time()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);

        $repository->set('foo', 'bar', 3600);
        $this->assertEquals('bar', $repository->get('foo'));
        Date::testing();
        Date::setNow((new DateTime())->add(\DateInterval::createFromDateString('3600 seconds')));
        $this->assertNull($repository->get('foo'));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_increment_and_decrement_cache_value()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);

        $repository->set('value', 5);

        $repository->increment('value');
        $this->assertEquals(6, $repository->get('value'));

        $repository->decrement('value');
        $this->assertEquals(5, $repository->get('value'));

        $repository->increment('value', 2);
        $this->assertEquals(7, $repository->get('value'));

        $repository->decrement('value', 2);
        $this->assertEquals(5, $repository->get('value'));
    }

    /**
     * @test
     * @group file
     */
    public function repository_can_remember_and_retrieve_exists_and_create_non_exists_data()
    {
        $manager = new CacheManager();
        $repository = $manager->resolve(CacheManager::FILE_DRIVER);

        $result = $repository->rememberForever('foo', function () {
            return 'bar';
        });

        $this->assertEquals('bar', $result);
        $this->assertEquals('bar', $repository->get('foo'));

        $repository->set('name', 'Pavel');

        $result = $repository->rememberForever('name', function () {
           return 'Finoghentov';
        });

        $this->assertEquals('Pavel', $result);

        $repository->remember('will_expire', 3600, function () {
            return 'expired';
        });

        Date::testing();
        Date::setNow((new DateTime())->add(\DateInterval::createFromDateString('3600 seconds')));

        $this->assertNull($repository->get('will_expire'));
    }
}
